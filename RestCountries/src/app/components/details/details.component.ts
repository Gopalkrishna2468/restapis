import { CountriesService } from './../../services/countries.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';



@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  countries: any[] = []
  selectedCountry: any[] = []
  country: any[] = []
  checkEqual: boolean = true
  constructor(private router: Router, private countriesService: CountriesService) { }

  ngOnInit(): void {
    this.countries = this.countriesService.countries
    this.selectedCountry = this.countriesService.selectedCountry
    for (let i = 0; i < 250; i++) {
      if (this.countries[i][1].name === this.selectedCountry[1].name) {
        this.country = this.countries[i]
      }
    }
  }
  backHome() {
    this.router.navigateByUrl('')
  }



}
