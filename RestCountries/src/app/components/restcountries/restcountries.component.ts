import { CountriesService } from './../../services/countries.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-restcountries',
  templateUrl: './restcountries.component.html',
  styleUrls: ['./restcountries.component.css']
})
export class RestcountriesComponent implements OnInit {
  @Input() transferToggle: any
  constructor(private countryService: CountriesService) {
    this.transferToggle = this.countryService.transferToggle
  }

  ngOnInit(): void {
  }
  onClickk(event: any) {
    this.countryService.onClickk(event)
  }
}

