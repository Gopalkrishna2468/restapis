import { CountriesService } from './../../services/countries.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  countries: any[] = []
  filterString: string = ''
  selectedRegion: string = ''
  constructor(private countriesService: CountriesService) {

  }

  ngOnInit(): void {
    this.countries = this.countriesService.countries
  }
  onSearch(event: Event) {
    let target = event.target as HTMLInputElement
    this.filterString = target.value
  }
  onSelectCountry(event: Event) {

    let target = event.target as HTMLInputElement
    this.selectedRegion = target.value
    if (this.selectedRegion == 'Filter by region')
      console.log(this.selectedRegion)
    console.log(this.countries)
    return this.countries
  }
}