import { CountriesService } from './../../services/countries.service';
import { Component, Input, OnInit } from '@angular/core';


@Component({
  selector: 'app-country-details',
  templateUrl: './country-details.component.html',
  styleUrls: ['./country-details.component.css']
})
export class CountryDetailsComponent implements OnInit {
  public countries: any[] = []
  selectedCountry: any[] = []
  public country: any;
  @Input() filterString: string = ''
  @Input() selectedRegion: string = ''
  constructor(private countriesService: CountriesService) {
    this.countriesService.onGet(this.countries)
    this.selectedCountry = this.countriesService.selectedCountry
  }

  ngOnInit(): void {
    this.countriesService.apiDataCall().subscribe(data => {
      Object.entries(data).map(item => {
        this.countries.push(item)
      })

    })


  }
  onClick(name: string) {
    this.countriesService.fetchCountry(name);
    console.log('data', this.selectedCountry)
  }
  detailPage(event: Event) {
    event.preventDefault()

  }
  onDelete(id: string) {
    this.country = []
    this.countries.find((x: any) => {
      if (x[1].numericCode == id)
        this.country = x;
    });
    console.log(this.country)
    let index = this.countries.indexOf(this.country, 0)
    console.log('jjt', this.countries.indexOf(this.country, 0))
    this.countries.splice(index, 1)



  }
}
