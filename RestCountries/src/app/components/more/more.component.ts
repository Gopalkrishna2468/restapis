import { CountriesService } from './../../services/countries.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-more',
  templateUrl: './more.component.html',
  styleUrls: ['./more.component.css']
})
export class MoreComponent implements OnInit {
  @Input() country: any[] = [];
  public transferToggle: any;
  event: any
  constructor(private countryServices: CountriesService) {
    this.transferToggle = this.countryServices.transferToggle
    this.event = this.countryServices.event
  }

  ngOnInit(): void {
    console.log(this.country)
  }
  onClickk(event: any) {
    this.countryServices.onClickk(event)
  }

}
