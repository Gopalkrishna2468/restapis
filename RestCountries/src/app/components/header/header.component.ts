import { CountriesService } from './../../services/countries.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  transferToggle: boolean = true
  constructor(private countriesService: CountriesService) {

  }

  ngOnInit(): void {
  }

}
