import { DetailsComponent } from './../components/details/details.component';
import { RestCountriesModule } from './restCountries.module';
import { CountriesService } from './../services/countries.service';
import { NgModule } from '@angular/core';
import { MoreComponent } from './../components/more/more.component';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';


const routes: Routes = [
    { path: '', component: MoreComponent }
]


@NgModule({
    declarations: [MoreComponent, DetailsComponent],
    imports: [CommonModule, RouterModule.forChild(routes), RestCountriesModule],
    providers: [CountriesService]
})


export class MoreDetailsModule {

}