import { FilterRegionPipe } from './../pipes/filter-region.pipe';
import { FilterPipe } from './../pipes/filter.pipe';
import { MoreComponent } from './../components/more/more.component';
import { CountriesService } from './../services/countries.service';
import { CountryDetailsComponent } from './../components/country-details/country-details.component';
import { SearchComponent } from './../components/search/search.component';
import { HeaderComponent } from './../components/header/header.component';
import { RestcountriesComponent } from './../components/restcountries/restcountries.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms';




const routes: Routes = [
    { path: '', component: RestcountriesComponent },
    { path: 'countries/:name', component: MoreComponent }

]

@NgModule({
    declarations: [RestcountriesComponent, HeaderComponent, SearchComponent, CountryDetailsComponent, FilterPipe, FilterRegionPipe],
    imports: [CommonModule, RouterModule.forChild(routes), HttpClientModule, FormsModule],
    providers: [CountriesService],
    exports: [HeaderComponent, CountryDetailsComponent]
})

export class RestCountriesModule {

}