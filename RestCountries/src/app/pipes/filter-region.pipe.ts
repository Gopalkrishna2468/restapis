import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterRegion'
})
export class FilterRegionPipe implements PipeTransform {

  transform(value: any[], filterString: string) {
    if (value.length === 0 || filterString === '') {
      return value;
    }
    const countries = value.filter(val =>
      val[1].name.toLowerCase().indexOf(filterString.toLowerCase()) !== -1);
    return countries;
  }

}
