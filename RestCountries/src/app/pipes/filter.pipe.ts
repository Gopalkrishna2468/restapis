import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, selectedRegion: string) {
    if (value.length === 0 || selectedRegion === '') {
      return value;
    }
    const countries = [];
    for (let country of value) {
      if (country[1].region === selectedRegion) {
        countries.push(country)
      }
    }
    return countries;
  }
}
