import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {
  public countries: any;
  public selectedCountry: any;
  public transferToggle: any;
  event: any
  constructor(private http: HttpClient) {

  }
  apiDataCall() {
    return this.http.get('https://restcountries.eu/rest/v2/all')
  }
  onGet(country: any) {
    this.countries = country;
  }
  getCountries() {
    return this.countries;
  }

  fetchCountry(countryName: any) {
    for (let i = 0; i < 250; i++) {
      console.log(this.countries[i]);
      let cname = this.countries[i][1].name

      if (cname === countryName) {
        this.selectedCountry = this.countries[i];
        console.log(this.selectedCountry)
      }
    }
  }
  onClickk(event: any) {
    this.event = event
    if (event.target.className == 'fa fa-moon-o') {
      event.target.className = 'fa fa-sun-o';
      event.target.textContent = 'Light Mode';
      event.target.parentNode.style.backgroundColor = 'yellow';
      event.target.parentElement.parentElement.parentElement.parentElement.className = 'changeDark'
      console.log(event.target.parentElement.parentElement.parentElement.parentElement)
      this.transferToggle = true
    }
    else {
      event.target.className = 'fa fa-moon-o';
      event.target.textContent = 'Dark Mode';
      event.target.parentNode.style.backgroundColor = 'white';
      event.target.parentElement.parentElement.parentElement.parentElement.className = 'brightMode'
      this.transferToggle = false
    }
  }

}
